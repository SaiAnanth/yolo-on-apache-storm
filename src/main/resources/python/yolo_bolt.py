import storm
from yolo import YOLO
from PIL import Image

class yoloBolt(storm.BasicBolt):
    def initialize(self, conf, context):
        self._conf = conf;
        self._context = context;
        self.yolo = YOLO()

    def process(self, tup):
        try:
            image = Image.open( tup.values[0])
        except:
            storm.emit(['Open Error! Try again!'])
        else:
            out = self.yolo.detect_image(image)
        for val in out:
            storm.emit([val["predicted_class"],str(val["score"]),str(val["left"]),str(val["top"]),str(val["right"]),str(val["bottom"]),str(tup.values[0])])

yoloBolt().run()
