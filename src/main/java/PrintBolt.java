import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.Map;

public class PrintBolt extends BaseRichBolt implements IRichBolt {
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {

    }

    public void execute(Tuple tuple) {
        StringBuilder sb = new StringBuilder();
        sb.append("predicted class : "+ tuple.getStringByField("predicted_class") +"\t");
        sb.append("score : "+ tuple.getStringByField("score") +"\t");
        sb.append("left : "+ tuple.getStringByField("left") +"\t");
        sb.append("top : "+ tuple.getStringByField("top") +"\t");
        sb.append("right : "+ tuple.getStringByField("right") +"\t");
        sb.append("bottom : "+ tuple.getStringByField("bottom") +"\t");
        sb.append("file name : "+ tuple.getStringByField("filename") +"\t");

        System.out.println(sb.toString());
    }

    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        System.out.println(tuple);
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
