import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.Map;

public class InputSpout extends BaseRichSpout implements IRichSpout {

    private TopologyContext context;
    private SpoutOutputCollector collector;
    private int id=0;
    private String[] inputFiles = {"dog.jpg","eagle.jpg","giraffe.jpg","kite.jpg","person.jpg","scream.jpg"};

    public void open(Map<String, Object> map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.context = topologyContext;
        this.collector = spoutOutputCollector;
    }

    public void nextTuple() {
        if(this.id<inputFiles.length){
            this.collector.emit(new Values(inputFiles[this.id]));
            this.id++;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(this.id == this.inputFiles.length){
            this.id=0;
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("name"));
    }

}
