import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class YOLOTopology {
    public static void main(String[] args) throws Exception{
        Config config = new Config();
//        config.setDebug(true);

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("Input-spout", new InputSpout());

        builder.setBolt("python-bolt", new pythonBolt("python3", "yolo_bolt.py"),2)
                .shuffleGrouping("Input-spout");

        builder.setBolt("output-bolt", new PrintBolt())
                .shuffleGrouping("python-bolt");


        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("Yolo_storm", config, builder.createTopology());
    }
}
