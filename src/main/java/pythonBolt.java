import org.apache.storm.task.ShellBolt;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;

import java.util.Map;

public class pythonBolt extends ShellBolt implements IRichBolt {
    public pythonBolt(String... command){
        super(command);
    }
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("predicted_class","score","left","top","right","bottom","filename"));
    }

    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
